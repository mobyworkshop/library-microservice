const {
    query
} = require('../data');

const {
    ServerError
} = require('../errors');

const {
    Book
} = require('./models.js');

const AuthorsService = require('../authors/services.js');
const { sendRequest } = require('../http-client');

const getAll = async () => {
    console.info('Getting all books...');

    const books = await query('SELECT b.id, b.nume, b.genre, a.id as id_autor, a.nume as nume_autor FROM books b JOIN authors a ON b.id_autor = a.id');

    return books.map(e => new Book(e));
};

const getById = async (id) => {
    console.info(`Getting book ${id}...`);

    const books = await query('SELECT b.id, b.nume, b.genre, a.id as id_autor, a.nume as nume_autor FROM books b JOIN authors a ON b.id_autor = a.id WHERE b.id=$1', [id]);

    if (books.length !== 1) {
        throw new ServerError('Book does not exist!', 404);
    }

    return new Book(books[0]);
};

const getAllForUser = async (id) => {
    console.info(`Getting all books for user ${id}...`);

    const books = await query('SELECT b.id, b.nume, b.genre, a.id as id_autor, a.nume as nume_autor FROM books b JOIN authors a ON b.id_autor = a.id WHERE b.id_user = $1', [id]);

    return books.map(e => new Book(e));
};

const add = async (nume, genre, id_autor, id_user) => {
    console.info(`${id_user} adauga o carte cu numele ${nume} si genul ${genre} scrisa de ${id_autor}`);

    try {
        const books = await query('INSERT INTO books (nume, genre, id_autor, id_user) VALUES ($1, $2, $3, $4) RETURNING id', [nume, genre, id_autor, id_user]);

        const autor = await AuthorsService.getById(id_autor);

        const options = {
            url: `http://${process.env.NOTIFICATION_SERVICE}/api/newsletter/notify`,
            method: 'POST',
            data: {
                bookName: nume,
                author: autor.nume
            }
        };

        await sendRequest(options);

        return books[0].id;
    } catch (e) {
        if (e.code === '23505') {
            throw new ServerError('Book already exists!', 409);
        } else if (e.code === '23503') {
            throw new ServerError(`Author with id ${id_autor} does not exist!`, 404)
        }
        throw e;
    }
};

const update = async (id, nume, genre, id_autor) => {
    console.info(`Modific numele cartii ${id} in ${nume}, genul in ${genre} si id_autor in ${id_autor}`);

    try {
        await query('UPDATE books SET nume=$1, genre=$2, id_autor=$3 WHERE id=$4', [nume, genre, id_autor, id]);
    } catch (e) {
        if (e.code === '23505') {
            throw new ServerError('Book already exists!', 409);
        } else if (e.code === '23503') {
            throw new ServerError(`Author with id ${id_autor} does not exist!`, 404)
        }
        throw e;
    }
};

const remove = async (id) => {
    console.info(`Sterg cartea ${id}...`);

    await query('DELETE FROM books WHERE id=$1', [id]);
}

const removeForUser = async (id) => {
    console.info(`Removing books for user ${id}...`);

    await query('DELETE FROM books WHERE id_user = $1', [id]);
};

module.exports = {
    getAll,
    getById,
    getAllForUser,
    add,
    update,
    remove,
    removeForUser
}