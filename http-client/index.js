const axios = require('axios').default;

const {
    ServerError
} = require('../errors');

const sendRequest = async (options) => {
    try {
        const { data } = await axios(options);
        return data;
    } catch (err) {
        if (err.isAxiosError) {

            console.error(err);
            throw new ServerError(err.response.data.message, err.response.status);
        }
        throw err;
    }
}

module.exports = {
    sendRequest
}
